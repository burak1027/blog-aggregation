package com.burak.blogaggregation;

import com.burak.blogaggregation.service.BlogLoadingService;
import io.swagger.client.api.CategoryLoadingControllerApi;
import io.swagger.client.api.PostLoadingControllerApi;
import io.swagger.client.api.UserLoadingControllerApi;
import io.swagger.client.model.PostDto;
import io.swagger.client.model.UserDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest

public class BlogLoadingTest {

    PostLoadingControllerApi postLoadingControllerApi;
    CategoryLoadingControllerApi categoryLoadingControllerApi;
    UserLoadingControllerApi userLoadingControllerApi;
    BlogLoadingService blogLoadingService;
    UserDto userDto;

    @Autowired
    public BlogLoadingTest(PostLoadingControllerApi postLoadingControllerApi, CategoryLoadingControllerApi categoryLoadingControllerApi, BlogLoadingService blogLoadingService) {
        this.postLoadingControllerApi = postLoadingControllerApi;
        this.categoryLoadingControllerApi = categoryLoadingControllerApi;
        this.blogLoadingService = blogLoadingService;

        Date date = new Date();
        userDto = new UserDto();
        userDto.setTcNo(46312956754L);
        userDto.setGender("Male");
        userDto.setName("Burak");
        userDto.setSurname("Gunden");
        userDto.setBirthPlace("Istanbul");
        userDto.setDateOfBirth(date.toString());
        userDto.setDeleted(false);
    }


    List<PostDto> getPostsByUsername(String username){
        userLoadingControllerApi = Mockito.mock(UserLoadingControllerApi.class);

        Mockito.when(userLoadingControllerApi.getUserByUserNameUsingGET(username)).thenReturn(userDto);

        return postLoadingControllerApi.getPostsOfAUserUsingGET(userLoadingControllerApi.getUserByUserNameUsingGET(username).getTcNo());
    }
    @Test
    void testGetPostsByUserName(){
        System.out.println(getPostsByUsername("bgunden"));
    }
    @Test
    void testGetPostByUserId(){
        System.out.println(postLoadingControllerApi.getPostsOfAUserUsingGET(userDto.getTcNo()));
    }
    @Test
    void testGetPostByCategory() throws Exception {
        System.out.println(blogLoadingService.getPostsByCategoryName("Furniture"));
    }





}
