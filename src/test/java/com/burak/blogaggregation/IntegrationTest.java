package com.burak.blogaggregation;

import com.burak.blogaggregation.Controller.BlogRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.client.api.CategoryLoadingControllerApi;
import io.swagger.client.api.PostLoadingControllerApi;
import io.swagger.client.api.UserLoadingControllerApi;
import io.swagger.client.model.Article;
import io.swagger.client.model.CategoryDto;
import io.swagger.client.model.PostDto;
import io.swagger.client.model.UserDto;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class IntegrationTest {

    MockMvc mockMvc;
    UserLoadingControllerApi userLoadingControllerApi;
    PostLoadingControllerApi postLoadingControllerApi;
    CategoryLoadingControllerApi categoryLoadingControllerApi;

    @Autowired
    public IntegrationTest(MockMvc mockMvc, UserLoadingControllerApi userLoadingControllerApi, PostLoadingControllerApi postLoadingControllerApi, CategoryLoadingControllerApi categoryLoadingControllerApi) {
        this.mockMvc = mockMvc;
        this.userLoadingControllerApi = userLoadingControllerApi;
        this.postLoadingControllerApi = postLoadingControllerApi;
        this.categoryLoadingControllerApi = categoryLoadingControllerApi;
    }

    public static String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String requestJson = mapper.writeValueAsString(object);
        return requestJson;

    }
    @Order(1)
    @Test
    void createPost() throws Exception{
        UserDto userDto = userLoadingControllerApi.getUserByUserNameUsingGET("bgunden");
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setCategoryName("Furniture");
        categoryDto.setId(1L);
        PostDto postDto = postLoadingControllerApi.getPostByPostIdUsingGET(1L);
        Article article = new Article();
        article.setContent("Some writings");
        postDto.setArticle(article);
        postDto.setId(5L);
        postDto.setPermaLink("life-in-Istanbul2");
        mockMvc.perform(MockMvcRequestBuilders.post("/blog")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(new BlogRequest(postDto,userDto,categoryDto))))
                .andDo(MockMvcResultHandlers.print());

    }
    @Order(2)
    @Test
    void updatePost() throws Exception {
        UserDto userDto = userLoadingControllerApi.getUserByUserNameUsingGET("bgunden");
        PostDto postDto = postLoadingControllerApi.getPostByPostIdUsingGET(3L);
        Article article = new Article();
        article.setContent("Updated Text");
        postDto.setArticle(article);
//        System.out.println(userDto.toString());
        String put = asJsonString(new BlogRequest(postDto,userDto));
        System.out.println(put);
        mockMvc.perform(MockMvcRequestBuilders.put("/blog")
                .contentType(MediaType.APPLICATION_JSON)
                .content(put))
                .andDo(MockMvcResultHandlers.print());

    }
    @Order(3)
    @Test
    void deletePost() throws Exception {
        UserDto userDto = userLoadingControllerApi.getUserByUserNameUsingGET("bgunden");
        PostDto postDto = postLoadingControllerApi.getPostByPostIdUsingGET(3L);
        String put = asJsonString(new BlogRequest(postDto,userDto));
        System.out.println(put);
        mockMvc.perform(MockMvcRequestBuilders.delete("/blog").
                contentType(MediaType.APPLICATION_JSON)
                .content(put))
                .andDo(MockMvcResultHandlers.print());

    }
    @Order(4)
    @Test
    void getPostByUsername() throws Exception {
        UserDto userDto = userLoadingControllerApi.getUserByUserNameUsingGET("bgunden");
        mockMvc.perform(MockMvcRequestBuilders.get("/blog/get-posts-by-username/bgunden"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].postCreatorId").value(userDto.getTcNo().toString()));
    }
    @Order(5)
    @Test
    void getDeletedPost() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/blog/post/life-in-Istanbul2"))
                .andDo(MockMvcResultHandlers.print());
    }
    @Order(6)
    @Test
    void getPostByPermalink() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/blog/post/life-in-istanbul"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.permaLink").value("life-in-istanbul"));
    }
    @Order(7)
    @Test
    void getPostsByCategory() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/post/post-category/Furniture"))
                .andDo(MockMvcResultHandlers.print());
    }


}
