package com.burak.blogaggregation.configuration;

import io.swagger.client.ApiClient;
import io.swagger.client.api.CategoryControllerApi;
import io.swagger.client.api.CategoryLoadingControllerApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CategoryConfig {
    @Bean
    public CategoryLoadingControllerApi categoryLoadingControllerApi(){
        return new CategoryLoadingControllerApi(apiClient());
    }
    @Bean
    public CategoryControllerApi categoryControllerApi(){
        return new CategoryControllerApi(apiClient());
    }
    public ApiClient apiClient() {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("http://localhost:8083");

        return apiClient;
    }

}
