package com.burak.blogaggregation.configuration;

import io.swagger.client.ApiClient;
import io.swagger.client.api.PostControllerApi;
import io.swagger.client.api.PostLoadingControllerApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PostConfig {
    @Bean
    public PostLoadingControllerApi postLoadingControllerApi(){
        return new PostLoadingControllerApi(apiClient());
    }
    @Bean
    public PostControllerApi postControllerApi(){
        return new PostControllerApi(apiClient());
    }
    public ApiClient apiClient() {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("http://localhost:8082");

        return apiClient;
    }
}
