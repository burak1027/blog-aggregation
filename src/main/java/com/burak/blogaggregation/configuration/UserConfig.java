package com.burak.blogaggregation.configuration;

import io.swagger.client.ApiClient;
import io.swagger.client.api.UserControllerApi;
import io.swagger.client.api.UserLoadingControllerApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfig {
    @Bean
    public UserLoadingControllerApi userLoadingControllerApi(){
        return new UserLoadingControllerApi(apiClient());
    }
    @Bean
    public UserControllerApi userControllerApi(){
        return new UserControllerApi(apiClient());
    }
    public ApiClient apiClient() {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("http://localhost:8080");

        return apiClient;
    }
}
