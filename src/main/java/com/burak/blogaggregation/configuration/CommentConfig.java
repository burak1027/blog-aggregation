package com.burak.blogaggregation.configuration;

import io.swagger.client.ApiClient;
import io.swagger.client.api.CommentControllerApi;
import io.swagger.client.api.CommentLoadingControllerApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatterBuilder;
import java.util.TimeZone;

@Configuration
public class CommentConfig {
    @Bean
    public CommentLoadingControllerApi commentLoadingControllerApi(){
        return new CommentLoadingControllerApi(apiClient());
    }
    @Bean
    public CommentControllerApi commentControllerApi(){
        return new CommentControllerApi(apiClient());
    }


    public ApiClient apiClient() {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("http://localhost:8081");

        return apiClient;
    }



}
//        DateFormat dateFormat= org.threeten.bp.format.DateTimeFormatterBuilder.getLocalizedDateTimePattern()

//        org.threeten.bp.format.DateTimeFormatterBuilder format = new org.threeten.bp.format.DateTimeFormatterBuilder();
//        format.append()
//        String pattern = "yyyy-MM-dd'";
//
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//        DateFormat dateFormat = new SimpleDateFormat(pattern);
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

// Use UTC as the default time zone.
//        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//        apiClient.setDateFormat(dateFormat);
//    @Bean
//    public DateTimeFormatterBuilder dateTimeFormatterBuilder() {
//        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
//        dateTimeFormatterBuilder.parseCaseInsensitive()
//                .appendPattern("hh:mm a")
//                .toFormatter();
//        return dateTimeFormatterBuilder;
//    }