package com.burak.blogaggregation.service;

import io.swagger.client.model.CategoryDto;
import io.swagger.client.model.PostDto;
import io.swagger.client.model.UserDto;

public interface BlogService {


     void createPost(PostDto postDto, UserDto userDto, CategoryDto categoryDto) throws Exception;
     void updatePost(PostDto postDto, UserDto userDto) throws Exception;
     void deletePost(PostDto postDto, UserDto userDto) throws Exception;

}
