package com.burak.blogaggregation.service;

import io.swagger.client.model.CategoryDto;
import io.swagger.client.model.PostDto;
import io.swagger.client.model.UserDto;

import java.util.ArrayList;
import java.util.List;

public interface BlogLoadingService {

    PostDto getPostById(long id);
    List<PostDto> getPostByUserId(long id);
    List<PostDto> getPostByCategoryId(long id);
    List<PostDto> getPostsByUserFullName(String name,String surname) throws Exception;
    List<PostDto> getPostsByCategoryName(String categoryName) throws Exception;
    PostDto getPostByPermalink(String permaLink);
    List<PostDto> getPostsByUsername(String username) throws Exception;
}
