package com.burak.blogaggregation.service;

import io.swagger.client.api.CategoryLoadingControllerApi;
import io.swagger.client.api.PostControllerApi;
import io.swagger.client.api.PostLoadingControllerApi;
import io.swagger.client.api.UserLoadingControllerApi;
import io.swagger.client.model.CategoryDto;
import io.swagger.client.model.PostDto;
import io.swagger.client.model.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlogServiceImpl implements BlogService{
    UserLoadingControllerApi userLoadingControllerApi;
    PostControllerApi postControllerApi;
    CategoryLoadingControllerApi categoryLoadingControllerApi;
    PostLoadingControllerApi postLoadingControllerApi;
    Logger logger = LoggerFactory.getLogger(BlogLoadingServiceImpl.class);


    @Autowired
    public BlogServiceImpl(UserLoadingControllerApi userLoadingControllerApi, PostControllerApi postControllerApi, CategoryLoadingControllerApi categoryLoadingControllerApi, PostLoadingControllerApi postLoadingControllerApi) {
        this.userLoadingControllerApi = userLoadingControllerApi;
        this.postControllerApi = postControllerApi;
        this.categoryLoadingControllerApi = categoryLoadingControllerApi;
        this.postLoadingControllerApi = postLoadingControllerApi;
    }

    public void createPost(PostDto postDto, UserDto userDto, CategoryDto categoryDto) throws Exception {
        userLoadingControllerApi.getUserByIdUsingGET(categoryDto.getId());
        UserDto userDto1;
        userDto1= userLoadingControllerApi.getUserByIdUsingGET(userDto.getTcNo());
        if(userDto1 == null){
            throw new Exception("Invalid user");
        }
        CategoryDto categoryDto1 = categoryLoadingControllerApi.getCategoryByIdUsingGET(categoryDto.getId());
        if(categoryDto1==null){
            throw new Exception("Invalid category");
        }
        if(postLoadingControllerApi.getPostByPostIdUsingGET(postDto.getId())!=null){
            throw new Exception("Post exist, please try to update existing post or create a new one!");
        }

        postDto.setCategoryId(categoryDto.getId());
        postDto.setPostCreatorId(userDto.getTcNo());
        postControllerApi.createPostUsingPOST(postDto);

    }
    public void updatePost(PostDto postDto, UserDto userDto) throws Exception{
        System.out.println(userDto.getTcNo() +" "+ postDto.getPostCreatorId());
        if (!userDto.getTcNo().equals(postDto.getPostCreatorId())){
            throw new Exception("The user is not the owner");
        }
        UserDto userDto1 = userLoadingControllerApi.getUserByUserNameUsingGET(userDto.getUserName());
        if(userDto1==null){
            throw new Exception("Invalid user");
        }

        if(postLoadingControllerApi.getPostByPostIdUsingGET(postDto.getId())==null){
            throw new Exception("Post did not exist");
        }
        postControllerApi.updatePostUsingPUT(postDto);
    }

    @Override
    public void deletePost(PostDto postDto , UserDto userDto) throws Exception {
        if(!postDto.getPostCreatorId().equals(userDto.getTcNo()))
            throw new Exception("Only the post owner can delete the post");
        postControllerApi.deletePostByPostIdUsingDELETE(postDto.getId());
    }

}
