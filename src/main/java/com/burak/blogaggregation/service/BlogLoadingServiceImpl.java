package com.burak.blogaggregation.service;

import io.swagger.client.api.CategoryLoadingControllerApi;
import io.swagger.client.api.PostLoadingControllerApi;
import io.swagger.client.api.UserLoadingControllerApi;
import io.swagger.client.model.CategoryDto;
import io.swagger.client.model.PostDto;
import io.swagger.client.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BlogLoadingServiceImpl implements BlogLoadingService {
    PostLoadingControllerApi postLoadingControllerApi;
    UserLoadingControllerApi userLoadingControllerApi;
    CategoryLoadingControllerApi categoryLoadingControllerApi;

    @Autowired
    public BlogLoadingServiceImpl(PostLoadingControllerApi postLoadingControllerApi, UserLoadingControllerApi userLoadingControllerApi, CategoryLoadingControllerApi categoryLoadingControllerApi) {
        this.postLoadingControllerApi = postLoadingControllerApi;
        this.userLoadingControllerApi = userLoadingControllerApi;
        this.categoryLoadingControllerApi = categoryLoadingControllerApi;

    }
    public PostDto getPostById(long id) {
        return postLoadingControllerApi.getPostByPostIdUsingGET(id);

    }
    public List<PostDto> getPostByUserId(long id)  {
        return postLoadingControllerApi.getPostsOfAUserUsingGET(id);
    }
    public List<PostDto> getPostByCategoryId(long id) {
        return postLoadingControllerApi.getPostsByCategoryIdUsingGET(id);
    }
    public List<PostDto> getPostsByUserFullName(String name,String surname) throws Exception {
        List<UserDto> userDto = userLoadingControllerApi.listUsersByFullNameUsingGET(name,surname);
       if(userDto==null)
           throw new Exception("User with user name "+name+" "+surname+" does not exist");

       List<PostDto> posts = new ArrayList<>();
       for(UserDto userDto1 : userDto){
           List<PostDto> postDtos = postLoadingControllerApi.getPostsOfAUserUsingGET(userDto1.getTcNo());
           postDtos.addAll(posts);
       }
        return posts;
    }
    public List<PostDto> getPostsByCategoryName(String categoryName) throws Exception{
        CategoryDto categoryDto = categoryLoadingControllerApi.getCategoryByNameUsingGET(categoryName);
        if (categoryDto==null)
            throw new Exception(categoryName+" does not exist");

        return postLoadingControllerApi.getPostsByCategoryIdUsingGET(categoryDto.getId());
    }
    public PostDto getPostByPermalink(String permaLink){
        PostDto postDto = postLoadingControllerApi.getPostByPermalinkUsingGET(permaLink);
        return postDto;
    }
    public List<PostDto> getPostsByUsername(String username) throws Exception{
        UserDto userDto = userLoadingControllerApi.getUserByUserNameUsingGET(username);
        if(userDto==null)
            throw new Exception("User with username "+username+" does not exist");
        return postLoadingControllerApi.getPostsOfAUserUsingGET(userDto.getTcNo());
    }

}
