package com.burak.blogaggregation;

import com.burak.blogaggregation.configuration.CommentConfig;
//import com.example.demo.service.BlogService;
import com.burak.blogaggregation.service.BlogServiceImpl;
import io.swagger.client.api.*;
import io.swagger.client.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(CommentConfig.class)
public class DemoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Autowired
    CommentLoadingControllerApi loadingControllerApi;
    @Autowired
    CommentControllerApi commentControllerApi;
    @Autowired
    PostLoadingControllerApi postLoadingControllerApi;
    @Autowired
    UserLoadingControllerApi userLoadingControllerApi;
    @Autowired
    CategoryLoadingControllerApi categoryLoadingControllerApi;
    @Autowired
    CategoryControllerApi categoryControllerApi;
    @Autowired
    BlogServiceImpl blogService;
    @Autowired
    UserControllerApi userControllerApi;
    @Override
    public void run(String... args)  {
//        CommentDto commentDto = loadingControllerApi.getCommentByCommentIdUsingGET(1L);
//        System.out.println(commentDto);
//        PostDto postDto = postLoadingControllerApi.getPostByPostIdUsingGET(1L);
//        System.out.println(postDto);
//        UserDto userDto1 = userLoadingControllerApi.getUserByIdUsingGET(46312956754L);
//        System.out.println(userDto1);
//
//        UserDto userDto2 = userLoadingControllerApi.getUserByUserObjectUsingGET(userDto1);
//        System.out.println(userDto2.toString());
//        System.out.println(userDto1.getDateOfBirth());

//        UserDto userDto1 = userLoadingControllerApi.getUserByUserObjectUsingGET(userDto);
//        System.out.println(userDto1);
//        userDto.setTcNo(294125904395L);
//        userDto.setBirthPlace("");
//        userDto.setBirthPlace("Ankara");
//        userControllerApi.updateUsingPUT(userDto);
//        userControllerApi.saveUsingPOST(userDto);
//        CategoryDto categoryDto = categoryLoadingControllerApi.getCategoryByIdUsingGET(1L);
//        PostDto postDto1 = new PostDto();
//        postDto1.setArticle(new Article().content("some text"));
//        postDto1.setTitle("Title");
        // userDto.setTcNo(18532095439L);
//        System.out.println(userDto.getBirthPlace());
//        userControllerApi.saveUsingPOST(userDto);
        //blogService.createPost(postDto1,userDto,categoryDto);

//
//        LocalDate date = LocalDate.now();
//        OffsetDateTime offsetDateTime = OffsetDateTime.now();
//        Date date1 = new Date();
//        System.out.println(date.toString());
//        UserDto userDto = new UserDto();
//        userDto.setTcNo(12456756897L);
//        userDto.setBirthPlace("LA");
//        userDto.setDateOfBirth(date.toString());
//        userDto.setName("Burak");
//        userDto.setSurname("Gunden");
//        userDto.setDeleted(false);
//        userDto.setGender("MALE");
//        userControllerApi.updateUsingPUT(userDto);

//        CommentDto commentDto = loadingControllerApi.getCommentByCommentIdUsingGET(1L);
//        commentDto.setContent("It was boring");
//        commentDto.setDateCreated(commentDto.getDateCreated());
//        commentControllerApi.updateCommentUsingPUT(commentDto);

//        CategoryDto categoryDto = new CategoryDto();
//        categoryDto.setCategoryName("Computers");
//        categoryDto.setId(123L);
//        categoryControllerApi.addCategoryUsingPOST(categoryDto);
    }
}
