package com.burak.blogaggregation.Controller;

import com.burak.blogaggregation.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/blog")
public class BlogController {
    @Autowired
    BlogService blogService;


    @PostMapping
    @ResponseBody
    public ResponseEntity<String> createPost(@RequestBody BlogRequest blogRequest) throws Exception {
        blogService.createPost(blogRequest.getPostDto(), blogRequest.getUserDto(), blogRequest.getCategoryDto());
        return ResponseEntity.ok("valid category");
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<String> updatePost(@RequestBody BlogRequest blogRequest) throws Exception {

        blogService.updatePost(blogRequest.getPostDto(), blogRequest.getUserDto());
        return ResponseEntity.ok("valid category");
    }

    @DeleteMapping
    @ResponseBody
    public ResponseEntity<String> deletePost(@RequestBody BlogRequest blogRequest) throws Exception {

        blogService.deletePost(blogRequest.postDto, blogRequest.getUserDto());
        return ResponseEntity.ok("valid category");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
