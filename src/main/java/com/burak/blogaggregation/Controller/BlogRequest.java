package com.burak.blogaggregation.Controller;

import io.swagger.client.model.CategoryDto;
import io.swagger.client.model.PostDto;
import io.swagger.client.model.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BlogRequest {
    PostDto postDto;
    UserDto userDto;
    CategoryDto categoryDto;

    public BlogRequest(PostDto postDto, UserDto userDto) {
        this.postDto = postDto;
        this.userDto = userDto;
    }
}
