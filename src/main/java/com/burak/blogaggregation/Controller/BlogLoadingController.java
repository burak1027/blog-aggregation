package com.burak.blogaggregation.Controller;

import com.burak.blogaggregation.service.BlogLoadingService;
import io.swagger.client.model.PostDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blog")
public class BlogLoadingController {
    @Autowired
    BlogLoadingService blogLoadingService;

    @GetMapping("/get-posts-by-username/{username}")
    List<PostDto> getPostsByUsername(@PathVariable("username") String userName) throws Exception {
        return blogLoadingService.getPostsByUsername(userName);
    }
    @GetMapping("/post/{permalink}")
    PostDto getPostByPermalink(@PathVariable("permalink") String permalink){
        return blogLoadingService.getPostByPermalink(permalink);
    }
    @GetMapping("/post-category/{categoryname}")
    List<PostDto> getPostsByCategoryName (@PathVariable("categoryname") String categoryName) throws Exception {
        return blogLoadingService.getPostsByCategoryName(categoryName);
    }
    @GetMapping("/category-id/{categoryid}")
    List<PostDto> getPostsByCategoryId(@PathVariable("categoryid") long categoryId){
        return blogLoadingService.getPostByCategoryId(categoryId);
    }
    @GetMapping("/get-post-by-id/{id}")
    PostDto getPostById(@PathVariable("id") long id){
        return blogLoadingService.getPostById(id);
    }
    @GetMapping("/get-posts-by-fullname")
    List<PostDto> getPostsByFullname(@RequestParam String name, @RequestParam String surname) throws Exception {
        return blogLoadingService.getPostsByUserFullName(name,surname);
    }

}
